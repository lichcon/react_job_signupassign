import React from 'react';
import '../node_modules/bootstrap/dist/css/bootstrap.min.css';
import logo from './logo.svg';
import './App.css';
import MainBody from './components/body'
import MainNavBar from './components/navbar'
import {Router} from 'react-router-dom';
import history from "./lib/my_history"
import { isUserRegistered } from './lib/my_auth';

class App extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      username: "",
      orgname: "",
      isAdmin: "",
      isLoggedIn: false
    };

    this.updateLoginStatus = this
      .updateLoginStatus
      .bind(this);
  }

  componentDidMount() {
    if(isUserRegistered("currentUser")){
      const user = JSON.parse(localStorage.getItem("currentUser"));
      this.setState({
        username: user.username,
        orgname: user.orgname,
        isAdmin: user.isadmin,
        isLoggedIn: true
      })
      
      
    }
    else{
      this.setState({
        username: "",
        orgname: "",
        isAdmin: "",
        isLoggedIn: false
      })
    }

  }

  updateLoginStatus(a_username, a_orgname, a_isAdmin, a_isLoggedIn) {
    if(isUserRegistered("currentUser")){
      const user = JSON.parse(localStorage.getItem("currentUser"));
      this.setState({
        username: user.username,
        orgname: user.orgname,
        isAdmin: user.isadmin,
        isLoggedIn: true
      })
      
    }
    else{
      this.setState({
        username: "",
        orgname: "",
        isAdmin: "",
        isLoggedIn: false
      })
    }
    console.log("login details update called")
  }

  render() {
    return (
      <Router history={history}>
        <div>
          <div>
            <MainNavBar loginState={this.state}  updateLogin={this.updateLoginStatus}/>
          </div>
          <div className="container pt-3">
            <MainBody  loginState={this.state} updateLogin={this.updateLoginStatus}/>
          </div>

        </div>
      </Router>
    );
  }
}

export default App;

{/* <div className="App">
      <header className="App-header">
        <img src={logo} className="App-logo" alt="logo" />
        <p>
          Edit <code>src/App.js</code> and save to reload.
        </p>
        <a
          className="App-link"
          href="https://reactjs.org"
          target="_blank"
          rel="noopener noreferrer"
        >
          Learn React
        </a>
      </header>
    </div> */
}
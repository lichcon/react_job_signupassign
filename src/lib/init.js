import { signup, org_addToList } from "./my_store";

//TODO: initialise local Storage
export function initialise_localStore() {
  // localStorage.clear();
  console.log("initialising....")
  const users = [
    { 
    "email": "test1@gmail.com",
    "password": "test@1",
    "username": "t001",
    "orgname": "org1",
    "isadmin": true,
    "dob": "2002-02-13",
    "fullname": "fullname001"
    },
    { 
      "email": "test2@gmail.com",
      "password": "test@2",
      "username": "t002",
      "orgname": "org2",
      "isadmin": false,
      "dob": "2002-02-23",
      "fullname": "fullname002"
    },
    { 
      "email": "test3@gmail.com",
      "password": "test@3",
      "username": "t003",
      "orgname": "org3",
      "isadmin": false,
      "dob": "2002-05-03",
      "fullname": "fullname003"
    },
    { 
      "email": "test4@gmail.com",
      "password": "test@4",
      "username": "t004",
      "orgname": "org3",
      "isadmin": false,
      "dob": "2005-03-03",
      "fullname": "fullname004"
    }
  ];

const orgs = {
    "org1" : [],
    "org2" : [],
    "org3" : []
  };

  const lists = {
    "users" : [],
    "orgs" : orgs
  }
  
  localStorage.setItem("Lists",JSON.stringify(lists));
  // for (let key in orgs) {
  //   org_addToList(key);
  // }
  users.forEach(user => {
    // console.log(localStorage.getItem("Lists"))
    console.log(signup(user,true));
  });
}
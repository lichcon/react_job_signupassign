//TODO: Make a mock Rest api 
//TODO: reformat error checking (alternative to returning a array)

import {login as userLogin, isUserRegistered} from "./my_auth";

export function getAllUser() {

  const lists =  JSON.parse(localStorage.getItem("Lists"));
  const users = lists["users"];
  const userObjList = [];
  users.forEach(function (user, index) {
    const username = localStorage.getItem(user);
    if (username === null) {
      console.log("getAllUser - " + username + " not found")
      return;
    }
    userObjList.push();
  }) 
}

export function signup(user, init = false) {

  const username = user.username;
  const password = user.password;
  const email = user.email;
  const orgname = user.orgname;

  if(isUserRegistered(username)) {
    return [false, "username already exists"];
  }
  else if (isUserRegistered(email)) {
    return [false, "email already exists"];
  }
  else {
    user_addToList(username, orgname);
    // maintaining 2 copies of each user mapped to email and username respectively, :/ bad design
    localStorage.setItem(username, JSON.stringify(user));
    localStorage.setItem(email, JSON.stringify(user));

    if(!init) {
      const userObj = userLogin(username,password);
      if (userObj[0]) {
        return [true, userObj[1]]
      }
      else {
        return [false, userObj[1]]
      }
    }
    else {
      return ;
    }
  }
} 

function user_addToList(username, orgname) {
  let myList = JSON.parse(localStorage.getItem("Lists"));

  myList['users'].push(username);
  myList['orgs'][orgname].push(username);
  // console.log(myList)
  
  localStorage.setItem("Lists", JSON.stringify(myList));
}

export function org_addToList(orgname) {
  
  // const list = localStorage.getItem("Lists");
  let myList = JSON.parse(localStorage.getItem("Lists"));
  myList['orgs'].orgname = [];
  localStorage.setItem("Lists", JSON.stringify(myList));

}

export function getUserList_byOrg(orgname) {
  const orgs = JSON.parse(localStorage.getItem("Lists"))['orgs'];
  // console.log(orgs)
  const userObjList = [];
  
  if(orgs.hasOwnProperty(orgname)){
    const users = orgs[orgname]
    // console.log(users)
    users.forEach(function (user, index) {
      const userObj = JSON.parse(localStorage.getItem(user));
      // console.log(userObj)
      if (userObj === null) {
        console.log("getUserByOrg - " + userObj.username + " not found")
        return;
      }
      userObjList.push(userObj);
    }) 
  }
  else {
    console.log("org not found")
  }
  // console.log(userObjList)
  return userObjList;
}

export function getOrgList() {
  const orgs = JSON.parse(localStorage.getItem("Lists"))['orgs'];
  return Object.keys(orgs);
}

export function updatePassword(username,password) {
  if(isUserRegistered(username)) {
    const user = JSON.parse(localStorage.getItem(username));
    user.password = password;
    localStorage.setItem(user.username, JSON.stringify(user));
    localStorage.setItem(user.email, JSON.stringify(user));
    return true;
  }
  else {
    alert("user not found");
    return [false, "user not found"];
  }
}
//TODO: Make a mock Rest api 
//TODO: reformat error checking (alternative to returning a array)
export function login(username, password) {
  // const options = {
  //     method: 'POST',
  //     headers: { 'Content-Type': 'application/json' },
  //     body: JSON.stringify({ username, password })
  // };
// username can  be email as well 
//  2 entries for each user signup 
// const userKey = username +"|"+ password;

  if(isUserRegistered(username)){
    console.log(username)

    const user = JSON.parse(localStorage.getItem(username));
    
    console.log(user.password)
      if (password.localeCompare(user.password) === 0) {
      // const orgName = userObj.orgname;
      // const userName = userObj.username;
      localStorage.setItem('currentUser', JSON.stringify(user));
      return [true, user];
    }
    return [false, "login failed - password mismatch"];
  }
  else {
    return [false, "login failed - username/email not registered"];
  } 
  
}

export function logout() {
  // remove user from local storage to log user out
  localStorage.removeItem('currentUser');
  return true;
}

export function isUserRegistered(username) {
  const user = localStorage.getItem(username);
  if (user !== null) {
    return true 
  }
  return false
}


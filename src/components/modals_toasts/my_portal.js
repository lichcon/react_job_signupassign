import React from 'react';
import ReactDOM from 'react-dom';

const portalRoot = document.getElementById('portal-root');

class Portal extends React.Component {
  constructor(props) {
    super(props);
    this.el = document.createElement('div');
  }

  componentDidMount() {
    // The portal element is inserted in the DOM tree after the Modal's children are
    // mounted, meaning that children will be mounted on a detached DOM node. If a
    // child component requires to be attached to the DOM tree immediately when
    // mounted, for example to measure a DOM node, or uses 'autoFocus' in a
    // descendant, add state to Modal and only render the children when Modal is
    // inserted in the DOM tree.
    portalRoot.appendChild(this.el);
  }

  componentWillUnmount() {
    portalRoot.removeChild(this.el);
  }

  render() {
    return ReactDOM.createPortal(this.props.children, this.el,portalRoot);
  }
}

export class Parent extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      clicks: 0
    };
    this.handleClick = this
      .handleClick
      .bind(this);
  }

  handleClick() {
    // This will fire when the button in Child is clicked, updating Parent's state,
    // even though button is not direct descendant in the DOM.
    this.setState(state => ({
      clicks: state.clicks + 1
    }));
  }

  render() {
    return (
      <div onClick={this.handleClick}>
        <Portal>
          {this.props.Child}  
        </Portal>
      </div>
      ); 
    } 
  } 
  export default Parent;
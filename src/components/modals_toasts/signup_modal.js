import React from 'react';
import {SignUp} from '../pages/signup'
import {Parent as ParentPortal} from './my_portal';

class SignupModal extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      clicks: 0
    };
    this.handleClick = this
      .handleClick
      .bind(this);
  }

  handleClick() {
    // This will fire when the button in Child is clicked, updating Parent's state,
    // even though button is not direct descendant in the DOM.
    this.setState(state => ({
      clicks: state.clicks + 1
    }));
  }

  SignupChild = () => 
  <div className="modal" id="myModal">
    <div className="modal-dialog">
      <div className="modal-content">

        <div className="modal-header">
          <h4 className="modal-title">Modal Heading</h4>
          <button type="button" className="close" data-dismiss="modal">&times;</button>
        </div>

        <div className="modal-body">
         sadasds
         {/* <SignUp isModal={true} hasState = {this.props.hasState} state={this.props.state} loginState = {{isLoggedIn : true}}/> */}
        </div>

        <div className="modal-footer">
          <button type="button" className="btn btn-danger" data-dismiss="modal">Close</button>
        </div>

      </div>
    </div>
  </div>

  render() {
    return (
      <ParentPortal Child = {<this.SignupChild/>}/>
    );
  }
}
export default SignupModal;
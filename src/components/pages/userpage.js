import React, {Component} from "react";
import {history} from '../../lib';
import {getOrgList, getUserList_byOrg} from "../../lib/my_store";
import SignupModal from "../modals_toasts/signup_modal";

class UserPage extends Component {
  constructor(props) {
    super(props);

    const isLoggedIn = this.props.loginState.isLoggedIn;
    if (!isLoggedIn) {
      // console.log(isLoggedIn)
      history.replace('/');
      // TODO: Route to Login
    }
    this.state = {
      orgname: "",
      selected_user: ""
    }
    this.adminOptions = this
      .adminOptions
      .bind(this);
    this.org_onChange = this.org_onChange.bind(this);
    this.radio_handleChange = this.radio_handleChange.bind(this);
    this.addUser_handleClick = this.addUser_handleClick.bind(this);
    this.deleteUser_handleClick = this.deleteUser_handleClick.bind(this);
    this.editUser_handleClick = this.editUser_handleClick.bind(this);

  }

  componentDidMount() {}

  addUser_handleClick() {
    console.log("addUser button clicked")
    //TODO: Signup in a modal
  }
  deleteUser_handleClick() {
    console.log("deleteUser button clicked")
    //TODO: delete selected user
  }
  editUser_handleClick() {
    console.log(this.state.selected_user)
    //TODO: signup modal with details filled in
  }

  radio_handleChange(e) {
    this.setState({
      selected_user : e.currentTarget.value
    })
    // console.log(this.state.selected_user);
  }

  org_onChange(e) {
    this.setState({orgname: e.target.value})
  }

  adminOptions() {
    if (this.props.loginState.isAdmin) {
      return <div className="col-sm-12 text-center">
        <button
          id="btnGrp"
          className="btn btn-success btn-md center-block"
          onClick={this.addUser_handleClick}>Add User</button>
        <button
          id="btnGrp"
          className="btn btn-danger btn-md center-block"
          onClick={this.deleteUser_handleClick}>Delete User</button>
        <button
          id="btnGrp"
          className="btn btn-info btn-md center-block"
          onClick={this.editUser_handleClick}>Edit User</button>
      </div>
    } else {
      return <div></div>
    }
  }

  OrgSelect = () => {
    const orgArray = getOrgList();
    let options = orgArray.map((orgname) => 
    <option key={orgname}>
      {orgname}
    </option>);

    return (
      <div className="dropdown">
        <div className="form-group">
          <label >Choose Organisation:</label>
          <select className="form-control" onChange={this.org_onChange}  value = {this.state.orgname}>
            <option></option>
            {options}
          </select>
        </div>
      </div>
    )
  }

  UserList = (props) => {
    // console.log(props.orgname)
    const userArray = getUserList_byOrg(props.orgname)
    // console.log(userArray);
    let entries = userArray.map((user) =>  
      <tr key = {user.username}>
        <td>
          <label className="form-group">
            <input type="radio" className="form-control" value= {user.username} 
            onChange = {this.radio_handleChange} 
            name="optradio"/>
          </label>
        </td>
        <td>{user.fullname}</td>
        <td>{user.username}</td>
        <td>{user.email}</td>
      </tr>
    )
    return (
      <div className="container">
        <table className="table table-striped">
          <thead>
            <tr>
              <th></th>
              <th>Fullname</th>
              <th>Username</th>
              <th>Email</th>
            </tr>
          </thead>
          <tbody>
            {entries}
          </tbody>
        </table>
      </div>
    )
  }

  render() {
    return (
      <div>
        <h3> Welcome {this.props.loginState.username}</h3>
        <form>
          <this.OrgSelect/>
          <this.UserList orgname={this.state.orgname}/>
        </form>
        <div className="row">
          <this.adminOptions/>
        </div>
        <div>
          <SignupModal/>
        </div>
      </div>
      
    );
  }
}

export default UserPage;
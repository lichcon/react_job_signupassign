import React, {Component} from "react";
import {Link} from 'react-router-dom';
import {history} from '../../lib';
import * as validate from '../../lib/my_validate';
import {signup as userSignup, getOrgList} from '../../lib/my_store'

export class SignUp extends Component {
  constructor(props) {
    super(props);
    const isLoggedIn = this.props.loginState.isLoggedIn;
    if (isLoggedIn) {
      history.replace('/userpage');
      // TODO: Route to userpage
    }

    this.handleClick = this
      .handleClick
      .bind(this);
    this.email_onChange = this
      .email_onChange
      .bind(this);
    this.password_onChange = this
      .password_onChange
      .bind(this);
    this.username_onChange = this
      .username_onChange
      .bind(this);
    this.fullname_onChange = this
      .fullname_onChange
      .bind(this);
    this.orgname_onChange = this
      .orgname_onChange
      .bind(this);
    this.dob_onChange = this
      .dob_onChange
      .bind(this);
    this.isAdmin_onChange = this
      .isAdmin_onChange
      .bind(this);

    this.state = {
      email: "",
      password: "",
      username: "",
      orgname: "",
      isadmin: false,
      dob: "",
      fullname: ""
    };
  }

  componentDidMount() {}

  email_onChange(e) {
    // TODO: email validation checks using mock api
    this.setState({email: e.target.value})
  }

  password_onChange(e) {
    //TODO: pass validation  checks using mock api
    this.setState({password: e.target.value})
  }

  fullname_onChange(e) {
    // TODO: email validation checks using mock api
    this.setState({fullname: e.target.value})
  }

  username_onChange(e) {
    this.setState({username: e.target.value})
  }

  dob_onChange(e) {
    //TODO: pass validation  checks using mock api
    this.setState({dob: e.target.value});
  }

  orgname_onChange(e) {
    // TODO: email validation checks using mock api
    this.setState({orgname: e.target.value});
  }

  isAdmin_onChange(e) {
    //TODO: pass validation  checks using mock api
    this.setState({isadmin: e.target.value});

  }

  handleClick() {
    console.log("signup initiated..")
    const user = {
      "email": this.state.email,
      "password": this.state.password,
      "username": this.state.username,
      "orgname": this.state.orgname,
      "isadmin": this.state.isadmin,
      "dob": this.state.dob,
      "fullname": this.state.fullname
    }

    // console.log(user);

    const signupDetails = userSignup(user);

    if (signupDetails[0]) {
      const user = signupDetails[1];
      // lift state up
      this
        .props
        .updateLogin(user.username, user.orgname, user.isadmin, true);
      history.push('/userpage');
    } else {
      alert(signupDetails[1])
    }

    //TODO: store the new user
    //TODO: lift state up
    console.log("signup finished")
    history.push('/userpage');
  }

  OrgSelect = () => {
    const orgArray = getOrgList();
    let options = orgArray.map((orgname) => <option key={orgname}>
      {orgname}
    </option>);

    return (
      <div className="dropdown">
        <div className="form-group">
          <label >Choose Organisation:</label>
          <select
            className="form-control"
            onChange={this.orgname_onChange}
            value={this.state.orgname}>
            <option></option>
            <option>Create New</option>
            {options}
          </select>
        </div>
      </div>
    )
  }

  SignupBody = () => 
    <div>
      <h3>Sign Up</h3>

      <div className="form-group">
        <label>Full Name</label>
        <input
          type="text"
          className="form-control"
          value={this.state.fullname}
          onChange={this.fullname_onChange}
          placeholder="Your Given Name"
          required/>
      </div>

      <div className="form-group">
        <label>Username</label>
        <input
          type="text"
          className="form-control"
          placeholder="1 letter followed by 3 digits, eg. a123"
          value={this.state.username}
          onChange={this.username_onChange}
          pattern={validate.regex.username}
          required/>
      </div>

      <div className="form-group">
        <label>Date of Birth</label>
        <input
          type="date"
          value={this.state.dob}
          onChange={this.dob_onChange}
          className="form-control"
          required/>
      </div>

      <this.OrgSelect/>

      <div className="form-group">
        <label>Email</label>
        <input
          type="email"
          className="form-control"
          placeholder="eg. test@gmail.com"
          value={this.state.email}
          onChange={this.email_onChange}
          pattern={validate.regex.email}
          required/>
      </div>

      <div className="form-group">
        <label>Password</label>
        <input
          type="password"
          className="form-control"
          placeholder="6 characters long "
          value={this.state.password}
          onChange={this.password_onChange}
          pattern={validate.regex.password}
          required/>
        <p>Must have atleast a number, a letter and a special character</p>
      </div>

      <div className="form-check">
        <label className="form-check-label">
          <input
            type="checkbox"
            className="form-check-input"
            value={true}
            onChange={this.isAdmin_onChange}/>Is Admin?
        </label>
      </div>

      <button type="submit" className="btn btn-primary btn-block">Sign Up</button>
      <p className=" text-right">
        Already registered?
        <Link to="/">
          Login here</Link>
      </p>
    </div>

  render() {
    return (
      <form onSubmit={this.handleClick}>
        <this.SignupBody />
      </form>
    );
  }
}

export default SignUp;
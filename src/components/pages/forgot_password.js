import React, {Component} from "react"
import {Link} from 'react-router-dom';
import {history} from '../../lib';
import * as validate from '../../lib/my_validate';
import { isUserRegistered } from "../../lib/my_auth";
import { updatePassword } from "../../lib/my_store";


class ForgotPassword extends Component {
  constructor(props) {
    super(props);

    this.handleClick = this
      .handleClick
      .bind(this);
    this.handleEmailSubmit = this
      .handleEmailSubmit
      .bind(this);  
    this.email_onChange = this
      .email_onChange
      .bind(this);
    this.password_onChange = this
      .password_onChange
      .bind(this);
    this.cpassword_onChange = this
      .cpassword_onChange
      .bind(this);

    this.state = {
      email: "",
      password: "",
      cpassword: "",
      err_msg : "",
      email_valid : false
    };

  /*   
    var visibility = false;
    var active_status = true;
    var submit_btn_text = "Next"; 
  */
  }

  cpassword_onChange(e) {
    this.setState({cpassword: e.target.value})
  }

  email_onChange(e) {
    this.setState({email: e.target.value})
  }

  password_onChange(e) {
    this.setState({password: e.target.value})
  }

  handleClick(e) {
    e.preventDefault();
    if(this.state.password.localeCompare(this.state.cpassword) === 0) {
      updatePassword(this.state.email,this.state.password);
      console.log("changed password saved");
      history.push('/');
    }
    else {
      alert("passwords mismatch");
    }
  }

  handleEmailSubmit(e) {
    // alert("hohoh")
    e.preventDefault();
    //TODO: auth
    if(isUserRegistered(this.state.email)) {
      this.setState({
        email_valid : true
      })
    } 
    else {
      alert("username does not exist")
    }
  }

  componentDidMount() {}

  render() {
    if (this.state.email_valid) {
      return (
        <form onSubmit={this.handleClick}>
          <h3>Forgot Password</h3>

          <div className="form-group">
            <label>Email address
            </label>
            <input
              type="text"
              className="form-control"
              value={this.state.email}
              onChange={this.email_onChange}
              placeholder="Enter email" 
              required disabled/>
          </div>
          
          <div className="form-group">
            <label>Password</label>
            <input
              type="password"
              className="form-control"
              value={this.state.password}
              onChange={this.password_onChange}
              pattern = {validate.regex.password}
              placeholder="Enter password" required/>
          </div>

          <div className="form-group">
            <label>Confirm Password</label>
            <input type="password" className="form-control" placeholder="Enter password"
            onChange={this.cpassword_onChange}
            pattern = {validate.regex.password} required/>
          </div>

          <button
            type="submit"
            className="btn btn-primary btn-block">Save</button>
          <p className=" text-right">
            <Link to = "/forgot_password" 
            onClick = {()=>{this.setState({email_valid : false}); }} >Back</Link>
          </p>

        </form>
      );
    }
    else {
      return (
        <form onSubmit={this.handleEmailSubmit}>
          <h3>Forgot Password</h3>

          <div className="form-group">
            <label>Email address
            </label>
            <input
              type="text"
              className="form-control"
              value={this.state.email}
              onChange={this.email_onChange}
              placeholder="Enter email" 
              required/>
          </div>
          <button
            type="submit"
            className="btn btn-primary btn-block">Next</button>
          <p className=" text-right">
            <Link to="/">Back</Link>
          </p>

        </form>
      );
    }
  }
}

export default ForgotPassword;
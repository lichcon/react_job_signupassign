import React, {Component} from "react";
import {Link} from 'react-router-dom';
import {history} from '../../lib';
import {login as userLogin} from "../../lib/my_auth"

class Login extends Component {
  constructor(props) {
    super(props);
    const isLoggedIn = this.props.loginState.isLoggedIn;
    console.log(isLoggedIn);
    if (isLoggedIn) {
      history.replace('/userpage');
    }
    this.handleClick = this
      .handleClick
      .bind(this);
    this.email_onChange = this
      .email_onChange
      .bind(this);
    this.password_onChange = this
      .password_onChange
      .bind(this);

    this.state = {
      email: "",
      password: ""
    };
    
    // 0 - none of the checks passed, 1 - one passed, 2 - both passed
    var checkStatus = 0;  
  }

  componentDidMount() {}

  email_onChange(e) {
    // TODO: email validation checks using mock api
    this.setState({email: e.target.value})
  }

  password_onChange(e) {
    //TODO: pass validation  checks using mock api
    this.setState({password: e.target.value})
  }

  handleClick(e) {
    e.preventDefault();
    console.log("login button clicked")
    // auth
    const loginDetails = userLogin(this.state.email,this.state.password);
    if (loginDetails[0]) {
      const user = loginDetails[1];
      // lift state up
      this.props.updateLogin(user.username, user.orgname, user.isadmin, true);
      history.push('/userpage');
    }
    else {
      alert(loginDetails[1])
    }
    
  }

  render() {
    return (
      <div>
        <form onSubmit={this.handleClick}>
          <h3>Sign In</h3>

          <div className="form-group">
            <label>Email address</label>
            <input
              type="text"
              className="form-control"
              value={this.state.email}
              onChange={this.email_onChange}
              placeholder="test@gmail.com"
              required />
          </div>

          <div className="form-group">
            <label>Password</label>
            <input
              type="password"
              className="form-control"
              value={this.state.password}
              onChange={this.password_onChange}
              placeholder="Enter password"
              required  />
          </div>

          <button
            type="submit"
            
            className="btn btn-primary btn-block">Login</button>
        </form>
        <div className = "row justify-content-between">
          <div className = "col-6">
            <p className=" text-left ">
              <Link to="/signup">Have not registered yet?</Link>
            </p>
            </div>
            <div className = "col-4">
            <p className=" text-right ">
              <Link to="/forgot_password">Forgot password?</Link>
            </p>
          </div>
        </div>
      </div>
    );
  }
}

export default Login;
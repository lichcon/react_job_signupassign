import React, {Component} from "react";
import {Link} from 'react-router-dom';
import {history} from '../lib';

class UserPage extends Component {
  constructor(props) {
    super(props);

    const isLoggedIn = this.props.isLoggedIn;
    if (!this.props.isLoggedIn) {
      console.log(isLoggedIn)
      history.replace('/');
    }

  }

  componentDidMount() {}

  handleSelection() {
    console.log("Org selected")
    //Todo: modal
  }

  render() {
    return (
      <div className="form-group">
        <label>Email address
        </label>
        <input
          type={this.props.type}
          className="form-control"
          value={this.props.value}
          onChange={this.props.onChange}
          placeholder={this.props.placeholder}/>
      </div>

    );
  }
}
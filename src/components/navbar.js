import React from 'react';
import {Link} from 'react-router-dom'
// import ReactDOM from 'react-dom';
import {history} from '../lib';
import {logout as userLogout} from '../lib/my_auth'

class MainNavBar extends React.Component {

  constructor(props){
    super(props);
    this.navbarButton = this.navbarButton.bind(this);
    this.logout_handleClick = this.logout_handleClick.bind(this);
  }

  login_handleClick() {
    console.log("login button clicked");
    history.push('/');
  }

  logout_handleClick() {
    console.log("login button clicked");
      userLogout();
      this.props.updateLogin();
    //TODO: state change
    history.push('/');
  }

  navbarButton() {
    const isLoggedIn = this.props.loginState.isLoggedIn;
    if(isLoggedIn){
      return <button
              type="submit"
              onClick={this.logout_handleClick}
              className="btn btn-primary btn-md">Logout</button>
    }
    else {
      return <div>
              {/* left blank */}
            </div>;
              // type="submit"
              // className="btn btn-primary btn-md"
              // onClick={this.login_handleClick} >Login
    }

  }
  
  render() {
    return (
      <header>
        <nav className="navbar navbar-dark fixed-top navbar-expand-lg">
          {/* <div>
            <ul className="nav navbar-nav" id="horizontal-list">
              <li className="nav-items">
                <Link to="/">Namaste</Link>
              </li>
              <li className="nav-items">
                <Link to="/Blog">Blog</Link>
              </li>
              <li className="nav-items">
                <Link to="/Proj">Proj</Link>
              </li>
            </ul>
          </div> */}
          <div className="col-md-12 text-right">
            <this.navbarButton/>
          </div>
        </nav>
      </header>
    )
  }
}
export default MainNavBar;
import React from 'react';
import {Route, Switch} from 'react-router-dom'
// import ReactDOM from 'react-dom';
import Login from './pages/login';
import SignUp from './pages/signup';
import ForgotPassword from './pages/forgot_password';
import UserPage from './pages/userpage';

class MainBody extends React.Component { 
  constructor(props) {
    super(props);
  }
  
  render() {
    return (
      <div id="content-body" className = "container">
        <Switch >
          <Route exact path= "/signup" component={()=> <SignUp loginState={this.props.loginState} updateLogin={this.props.updateLogin} />}/>
          <Route exact path= "/" component={()=> <Login loginState={this.props.loginState} updateLogin={this.props.updateLogin} />}/>
          <Route exact path= "/forgot_password" component={()=> <ForgotPassword/>}/>
          <Route exact path= "/userpage" component={()=> <UserPage loginState={this.props.loginState} />}/>
        </Switch>
      </div>
    )
  }
}
export default MainBody
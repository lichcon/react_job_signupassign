import React, {Component} from "react";
import {Link} from 'react-router-dom';
import {history} from '../lib';

class UserPage extends Component {
  constructor(props) {
    super(props);

    const isLoggedIn = this.props.isLoggedIn;
    if (!this.props.isLoggedIn) {
      console.log(isLoggedIn)
      history.replace('/');
    }

  }

  componentDidMount() {}

  handleSelection() {
    console.log("Org selected")
    //Todo: modal
  }

  render() {
    return (
      <div className="dropdown">
        <div className="form-group">
          <label >Choose Organisation:</label>
          <select onSelect={this.handleSelection} className="form-control">
            <option>1</option>
            <option>2</option>
            <option>3</option>
            <option>4</option>
          </select>
        </div>
      </div>
    );
  }
}